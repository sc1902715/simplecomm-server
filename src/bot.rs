use std::any::Any;
use std::error::Error;
use std::io::{stdout, Write};
use std::sync::Mutex;

use async_openai::config::{self, Config, OpenAIConfig};
//OpenAI Libraries
use async_openai::types::ChatCompletionRequestUserMessageArgs;
use async_openai::types::ChatCompletionRequestMessage;
use async_openai::{types::CreateChatCompletionRequestArgs,types::ChatCompletionRequestSystemMessageArgs,types::ChatCompletionRequestAssistantMessageArgs, types::CreateChatCompletionRequest, Client};
use futures::StreamExt;

// //Send the query with conversation history to bot and get the reponse
// pub fn ask_bot(
//     text_from_chat: String,
//     conversation_history: &mut Vec<String>,
//     model_tokenizer_device: ModelTokenizerDevice,
//     inference_args: InferenceArgs,
// ) -> String {
//     let history_split = conversation_history
//         .iter()
//         .rev()
//         .take(2)
//         .rev()
//         .map(|x| x.to_string())
//         .collect::<Vec<String>>();

//     // Create prompt

//     let role = inference_args.role.as_ref().map(|r| r.to_string());
//     let prompt = TemplateGenerator::generate(
//         &text_from_chat,
//         history_split,
//         &model_tokenizer_device.template,
//         inference_args.load_context,
//         role,
//     );

//     println!(
//         "\n{:?}\nPrompt:\n{}\n\n",
//         &model_tokenizer_device.template, prompt
//     );

//     // Produce response from Mistral
//     //let bot_response = start(prompt, model_tokenizer_device.clone(), &inference_args)
//     //    .unwrap_or("BOT_ERROR".to_string());

//     //Produce response from OpenAI call connect_with_open_ai()

//     // Parse Bot Response with Regex
//     let regex = Regex::new(r#"(<\|im_end\|>|<\|/im_end\|>|\|im_end\||<\||assistant\n|\[INST\]|\[\/INST\]|\[inst\]|\[\/inst\]|<\/s>|<\/S>|\n\n### User:|\"\n\n|\nB:)"#)
//         .expect("Invalid regex");
//     let split_bot_response = regex
//         .split(&bot_response)
//         .take(1)
//         .map(|x| x.to_string())
//         .collect::<Vec<String>>()
//         .join("");

//     // add to conversation_history
//     conversation_history.push(text_from_chat);
//     conversation_history.push(split_bot_response.clone());

//     // Send bot message
//     split_bot_response.clone().to_string()
// }

// lazy_static! {
//     static ref OPEN_AI_CLIENT: async_openai::Client<OpenAIConfig> = Client::new();
//     pub static ref CHAT_COMPLETION_REQUEST_MESSAGES: Vec<ChatCompletionRequestMessage> = Vec::new();
// }


// //Call OpenAI servers and get connection ready for the model to respond to our prompts
pub async fn ask_open_ai(query: Vec<ChatCompletionRequestMessage>) -> String {
    let client = Client::new();
    let request = match CreateChatCompletionRequestArgs::default()
        .model("gpt-3.5-turbo")
        .max_tokens(512u16)
        .messages(query)
        .build()
    {
        Ok(req) => req,
        Err(_) => return "Failed to build request".to_string(),
    };

    let response = match client.chat().create(request).await {
        Ok(resp) => resp,
        Err(_) => return "Failed to get response from OpenAI".to_string(),
    };
    log::info!("\nResponse:\n");
    // Assume we are only interested in the first choice's content
    let reply = response.choices.into_iter().next()
        .map(|choice| choice.message.content)
        .unwrap_or_else(|| Some("No response choices found".to_string()));
    let send_reply:String = reply.unwrap();
        return send_reply;
}