use std::io::Bytes;
use std::time::{Duration, Instant};
use actix::prelude::*;
use actix_web_actors::ws;
use std::sync::Mutex;
use std::mem;
use serde::{Serialize, Deserialize};
use serde_json;
use std::str;
use std::error::Error;
//OpenAI Libraries
use async_openai::types::ChatCompletionRequestUserMessageArgs;
use async_openai::types::ChatCompletionRequestMessage;
use async_openai::{types::CreateChatCompletionRequestArgs,types::ChatCompletionRequestSystemMessageArgs,types::ChatCompletionRequestAssistantMessageArgs, types::CreateChatCompletionRequest};

use crate::bot;

// Handle this websocket request
//{"HX-Request":"true","HX-Trigger":"ask_ai","HX-Trigger-Name":null,"HX-Target":"ask_ai","HX-Current-URL":"http://127.0.0.1:8000/"}
#[derive(Serialize, Deserialize, Debug)]
struct HeaderData {
    #[serde(rename = "HX-Request")]
    hx_request: String,
    #[serde(rename = "HX-Trigger")]
    hx_trigger: String,
    #[serde(rename = "HX-Trigger-Name")]
    hx_trigger_name: Option<String>,
    #[serde(rename = "HX-Target")]
    hx_target: String,
    #[serde(rename = "HX-Current-URL")]
    hx_current_url: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct QueryResponse {
    message: String,
    #[serde(rename = "HEADERS")]
    headers: HeaderData,
}

fn get_query_from_json (byte_slice: &[u8]) -> String {
                match str::from_utf8(byte_slice) {
                    Ok(json_str) => {
                     // Deserialize the JSON string into a Text object
                        match serde_json::from_str::<QueryResponse>(json_str) {
                            Ok(query) => {
                                log::info!("Deserialized Text: {:?}", query);
                                return query.message
                            },
                            Err(e) => {
                                log::error!("Error deserializing: {}", e);
                                return String::from("bad_esponse");
                                },
                        }
                    }
                Err(e) => {
                                log::error!("Error deserializing: {}", e);
                                return String::from("bad_esponse");
                                },
                }
} //TODO: split into 2 fns parse_json() and get_query_from_json()


/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);


pub struct MyWebSocket {
    /// Client must send ping at least once per 10 seconds (CLIENT_TIMEOUT),
    /// otherwise we drop connection.
    hb: Instant,
    user_id: i32,
    conversation: Mutex<Vec<ChatCompletionRequestMessage>>,
}

impl MyWebSocket {
    pub fn new(id: i32) -> Self {
        Self { 
            hb: Instant::now() ,
            user_id: id,
            conversation: Mutex::new(Vec::new()),
        }
    }
    
    fn add_message_to_chat(&self, content: &str, role: &str) -> Result<(), Box<dyn Error>> {
        //let mut messages = self.conversation.clone();
    
        let new_message = match role {
            "system" => ChatCompletionRequestSystemMessageArgs::default()
                .content(content)
                .build()?
                .into(),
            "user" => ChatCompletionRequestUserMessageArgs::default()
                .content(content)
                .build()?
                .into(),
            "assistant" => ChatCompletionRequestAssistantMessageArgs::default()
                .content(content)
                .build()?
                .into(),
            _ => return Err("Invalid role specified".into()),
        };

        let mut conversation = self.conversation.lock().unwrap();
        conversation.push(new_message);
    //CHAT_COMPLETION_REQUEST_MESSAGES = messages;
        Ok(())
    }
        

    /// helper method that sends ping to client every 5 seconds (HEARTBEAT_INTERVAL).
    ///
    /// also this method checks heartbeats from client
    fn hb(&self, ctx: &mut <Self as Actor>::Context) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                log::info!("Websocket Client heartbeat failed, disconnecting!");
                
                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            ctx.ping(b"");
        });
    }
}

impl Actor for MyWebSocket {
    type Context = ws::WebsocketContext<Self>;

    /// Method is called on actor start. We start the heartbeat process here.
    fn started(&mut self, ctx: &mut Self::Context) {
        self.hb(ctx);
        log::info!("Websocket started successfully");
        
        //Populate the stadard opening lines of the ChatBot
        //TODO: Handle Error
        self.add_message_to_chat("You are a helpful assistant.","system"); 
        self.add_message_to_chat("Who art thou","user");
    }
}

/// Handler for `ws::Message`
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWebSocket {
    
    fn handle(&mut self, msg: Result<ws::Message, ws::ProtocolError>, ctx: &mut Self::Context) {
        // process websocket messages
        log::debug!("WS: {msg:?}");
        match msg {
            Ok(ws::Message::Ping(msg)) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            }
            Ok(ws::Message::Pong(_)) => {
                self.hb = Instant::now();
            }
            Ok(ws::Message::Text(text)) =>  {
                let text_byte: &[u8] = text.as_ref(); //Idk why but I need to change it to u8 string for it to work
                log::info!("WS received a message: {text}");  
                // Typical Response: {"message":"what a wonderful day","HEADERS":{"HX-Request":"true","HX-Trigger":"ask_ai","HX-Trigger-Name":null,"HX-Target":"ask_ai","HX-Current-URL":"http://127.0.0.1:8000/"}}
                let query = get_query_from_json(text_byte);
                log::info!("Query : {query}");
                self.add_message_to_chat(query.as_str(),"user");
                let mut locked_data = self.conversation.lock().unwrap();
                let vec: Vec<ChatCompletionRequestMessage> = mem::take(&mut *locked_data);
                
                //let ctx_address = ctx.address();

                // Call the ask_open_ai function and handle the result asynchronously
                actix::spawn(async move {
                    let response = bot::ask_open_ai(vec);
                            log::info!("The request to OpenAI was successful!");
                            //ctx_address.do_send();
                            //ctx.text(response.await.into_bytes());
                       
                });
                
            },
            Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            Ok(ws::Message::Close(reason)) => {
                ctx.close(reason);
                ctx.stop();
            }
            _ => ctx.stop(),
        }
    }
}