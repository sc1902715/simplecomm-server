pub mod server;
pub mod bot;
use self::server::MyWebSocket;
//use std::error::Error;
use std::io::{stdout, Write};

//OpenAI Libraries
use async_openai::types::ChatCompletionRequestUserMessageArgs;
use async_openai::{types::CreateChatCompletionRequestArgs, Client};
use futures::StreamExt;

// Basic actix webserver
use actix_web::{get,web::Data, App, HttpRequest, HttpResponse, HttpServer, Responder, Error};
use actix_web::web;
use tera::{Context, Tera};
use actix_web_actors::ws;
use std::sync::{Arc, Mutex};
#[macro_use]
extern crate lazy_static;


//Base URL Route Function
#[get("/")]
async fn home(tera: Data<Tera>) -> impl Responder {
    log::debug!("Home called with tera Object {:?}", tera);
    HttpResponse::Ok().body(tera.render("main.html", &Context::new()).unwrap())
    
}

//Request to open the websocket connection
#[get("/comm")]
async fn comm(tera: Data<Tera>, req: HttpRequest, stream: web::Payload) -> Result<HttpResponse, actix_web::Error> {
    log::info!("Opening Websocket Connection");
    ws::start(MyWebSocket::new(24601), &req, stream) //TODO: implement userID 
}

// Main function
#[tokio::main]
async fn main() -> std::io::Result<()> {
    env_logger::init();
    log::info!("Starting #Bot_Name# Server"); //TODO: Add constant for Bot_Name which will be called whenever required in logs
    //let server = Arc::new(Chat::new());
    let tera = Data::new(Tera::new("./src/templates/**/*.html").unwrap());

    HttpServer::new(move || {App::new()
                                .app_data(tera.clone())
                                .service(home) // Will execute fn home() when website is called
                                .service(comm) // will execute fn comm() when called to open the websocket
                            })
        .bind(("127.0.0.1", 8000))?
        .run()
        .await
    
}
